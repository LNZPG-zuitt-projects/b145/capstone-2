const Product = require('../models/Product');

// Create Post (must be Loggged in as Admin)
module.exports.addProduct = async(data) => {
    console.log(data)
        // User is an Admin
    if (data.isAdmin) {

        // Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
        // Uses the information from the request body to provide all the necessary information
        let newProduct = new Product({
            name: data.product.name,
            description: data.product.description,
            price: data.product.price
        });


        // saves the created object to our database
        return newProduct.save().then((product, error) => {

            // Course creation successful
            if (error) {
                return false;

                // Course creation failed
            } else {
                return `Product added 
                ${product}`;
            };
        });


        // User is not an admin
    } else {
        return false;
    }
};


// Retrieveing all products
module.exports.getAllProducts = async(user) => {
    if (user.isAdmin === true) {
        return Product.find({}).then(result => {
            return result
        })
    } else {
        return `Ooops, ${user.email} is not authorized`
    }
};


// Archive Product (makes Product inactive)
module.exports.archiveProduct = async(data) => {

    if (data.payload.isAdmin === true) {

        return Product.findById(data.productId).then((result, err) => {

            result.isActive = false;

            return result.save().then((archivedProduct, err) => {

                if (err) {
                    return false;
                } else {
                    return result;
                };
            });
        });

    } else {
        return false;
    };
};


// Retrieval of active courses
module.exports.getAllActive = () => {
    return Product.find({ isActive: true }).then(result => {
        return result
    });
};


// retrieval of a specific course
module.exports.getProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {
        return result
    });
};


// Updating a course (error)
module.exports.updateProduct = (data) => {
    console.log(data);
    return Product.findById(data.productId).then((result, err) => {

        if (data.payload.isAdmin === true) {

            let updatedProduct = {
                name: data.updatedProduct.name,
                description: data.updatedProduct.description,
                price: data.updatedProduct.price,
            }

            return data.updatedProduct.save().then((updatedProduct, err) => {
                if (err) {
                    return false
                } else {
                    return updatedProduct
                }
            })
        } else {
            return false
        }
    })
};