const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
// const Product = require('../models/Product');


// User Registration
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, err) => {
        if (err) {
            return false
        } else {
            return user
        }
    })
};

// Login
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {

            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(
                reqBody.password, result.password)

            if (isPasswordCorrect) {

                return {
                    access: auth.createAccessToken(
                        result)
                }

            } else {

                return false

            }
        }
    })
};

// Retrieve specific details
module.exports.getProfile = async(data) => {

    const result = await User.findById(data.userId);

    result.password = "";
    return result;

};

// Login (admin)
module.exports.setAdmin = async(data) => {
    console.log(data)
    return User.findById(data.userId).then((result, err) => {
        if (data.payload.isAdmin) {

            console.log(result)
                // result.isAdmin = data.result.isAdmin
            return result.save().then((setAdmin, err) => {
                if (err) {
                    return err
                } else {
                    return setAdmin
                }
            })

        } else {
            return false
        }
    })
};


// Order
module.exports.order = async(data) => {
    console.log(data)
    if (data.isAdmin === true) {

        return `Only regular user can order`

    } else {

        let isUserUpdated = await User.findById(data.userId).then(user => {

            user.enrollments.push({ productId: data.productId })

            return user.save().then((user, err) => {
                if (err) {
                    return false
                } else {
                    return true
                }
            })
        });

        let isOrderPlaced = await Product.findById(data.productId).then(product => {
            product.enrollees.push({ userId: data.userId })

            return product.save().then((product, err) => {

                if (err) {

                    return false

                } else {

                    return true
                }
            })
        })

        if (isUserUpdated && isOrderPlaced) {

            return `Ordered successfully`

        } else {

            return `Try again`
        }
    }
};