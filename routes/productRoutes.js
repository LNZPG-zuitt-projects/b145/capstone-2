const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');


// Create Post (must be Loggged in as Admin)
router.post('/', auth.verify, (req, res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.addProduct(data).then(resultFromController => res.send(resultFromController))
});


// Retrieve all products
router.get('/all', auth.verify, (req, res) => {
    const data = auth.decode(req.headers.authorization)

    productController.getAllProducts(data).then(resultFromController => res.send(resultFromController))
});


// Archive Product (makes Product inactive)
router.put('/:productId/archive', auth.verify, (req, res) => {

    const data = {
        productId: req.params.productId,
        payload: auth.decode(req.headers.authorization)
    }

    productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});


// Retrieval of active courses
router.get('/', (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// retrieval of specific course
router.get('/:productId', (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update a course (with error)
router.put('/:productId/update', auth.verify, (req, res) => {

    const data = {
        productId: req.params.productId,
        payload: auth.decode(req.headers.authorization),
        updatedProduct: req.body
    }

    productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
});


module.exports = router;