const express = require('express');
const req = require('express/lib/request');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/userControllers');

// User Registration
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Login
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Retrieve specific details
router.post('/details', auth.verify, (req, res) => {
    // Provides the user's ID for the getProfile controller method
    const userData = auth.decode(req.headers.authorization)

    userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});

// Login admin
router.put('/:userId/admin', auth.verify, (req, res) => {
    const data = {
        userId: req.body.userId,
        payload: auth.decode(req.headers.authorization),
        setAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
})


// Order

router.post("/order", auth.verify, (req, res) => {
    let data = {
        name: req.body.productId,
        quantity: req.body.quantity,
        totalAmount: req.body.totalAmount
    }

    userController.order(data).then(resultFromController => res.send(resultFromController))
})



module.exports = router;