// [SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
// const orderRoutes = require('./routes/orderRoutes');

// [SECTION] Environment Variables Setup
// configure the application inn order for it too recognize an identify the necessary components neede to build the app succesfully
dotenv.config();
// extract the values from the .env file
const secret = process.env.CONNECTION_STRING;

// [SECTION] Server Setup
const port = process.env.PORT;
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


// [SECTION] DATABASE CONNECT
// Connection String
mongoose.connect(secret, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log("Successfully connected to MongoDB"));

// [SECTION] SERVER ROUTES
app.use('/users', userRoutes);
app.use('/product', productRoutes);
// app.use('/order', orderRoutes);


// [SECTION] Server Response
app.listen(port, () => console.log(`Server running at port ${port}`));