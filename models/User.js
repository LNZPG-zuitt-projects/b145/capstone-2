const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

    email: {
        type: String,
        required: [true, "Email is required"]
    },

    password: {
        type: String,
        required: [true, "Password is required"]
    },

    isAdmin: {
        type: Boolean,
        default: false
    },


    order: [{

        userId: {
            type: String,
            required: [true, "Please input User ID"]
        },

        productId: {
            type: String,
            required: [true, "Please input Product ID"]

        },
        quantity: {
            type: Number,
            required: [true, "Please input quantity"]
        },

        totalAmount: {
            type: Number,
            required: [true, "Please input total amount"]
        },

        purchasedOn: {
            type: Date,
            default: new Date()
        },
    }]
});

module.exports = mongoose.model('User', userSchema);